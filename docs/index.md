# 분류를 위한 서포트 벡터 머신 <sup>[1](#footnote_1)</sup>

> <font size="3">분류 작업에 서포트 벡터 머신을 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [들어가며](./support-vector-machines.md#intro)
1. [서포트 벡터 머신이란?](./support-vector-machines.md#sec_02)
1. [서포트 벡터 머신은 어떻게 작동할까?](./support-vector-machines.md#sec_03)
1. [서포트 벡터 머신의 훈련 방법](./support-vector-machines.md#sec_04)
1. [분류를 위해 서포트 벡터 머신 사용법](./support-vector-machines.md#sec_05)
1. [서포트 벡터 머신의 장단점](./support-vector-machines.md#sec_06)
1. [마치며](./support-vector-machines.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 5 — Support Vector Machines for Classification](https://code.likeagirl.io/ml-tutorial-5-support-vector-machines-for-classification-a3674a1e232f?sk=582f790e1fdfe1f32dbf6190a78ce949)를 편역하였다.
